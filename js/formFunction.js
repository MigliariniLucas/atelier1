$(document).ready(function () {
  $("form").submit(function (event) {
    var formData = {
      name: $("#name").val(),
      description: $("#description").val(),
      family: $("#family").val(),
      affinity: $("#affinity").val(),
      imgUrl: $("#imgUrl").val(),
      smallImgUrl: $("#imgUrl").val(),
      id: 0,
      energy: $("#energy").val(),
      hp: $("#hp").val(),
      defence: $("#defence").val(),
      attack: $("#attack").val(),
      price: 0,
      userId: 0
    };


    $.ajax({
      type: "POST",
      url: "http://vps.cpe-sn.fr:8083/card",
      data: JSON.stringify(formData),
      processData: false,
    contentType: 'application/json',
    }).done(function (data) {
      console.log(data);
    });

    event.preventDefault();
  });
});
